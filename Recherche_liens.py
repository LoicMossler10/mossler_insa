import requests
import re

def recherche_liens(page):
    "Fonction qui recherche et affiche les liens présents sur une page web"
    r = requests.get(page)
    urls = re.findall('http[s]?://[^"]+', r.text)
    n = len(urls)
    print ("\nVoici les " + str(n) + " liens trouvés sur " + page + " :\n")
    print (urls)
    return urls

def calcul_taille(liste):
    "Fonction qui calcule le nombre d'éléments d'une liste"
    taille = len(liste)
    return taille

"""Demande une page web à l'utilisateur"""
page_choisie = input("Entrer l'adresse d'une page web : ")
"""Recherche et affiche les liens de la page choisie"""
liens = recherche_liens(page_choisie)
"""Calcule le nombre de liens présents sur la page choisie"""
nombre_liens = calcul_taille(liens)
"""Recherche et affiche les liens des pages liées à la page choisie"""
for i in range (0,nombre_liens):
    print("\n" + str(i+1))
    recherche_liens(liens[i])